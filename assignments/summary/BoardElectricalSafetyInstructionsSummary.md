# Board Electrical Safety Instructions

## Dos and Don'ts

### Power Supply

#### Dos:

 - Always make sure that the output voltage of the power supply matches the input voltage of the board.
 - While turning on the power supply make sure every circuitbOARDis connected properly.

#### Don’ts:

 - Do not connect power supply without matching the power rating.
 - Never connect a higher output(12V/3Amp) to a lower (5V/2Amp) input.
 - Do not try to force the connector into the power socket ,it may damage the connector.

### Handling

#### Dos:

 - Treat every device like it is energized, even if it does not look like it is plugged in or operational.
 - While working keep the board on a flat stable surface (wooden table).
 - Unplug the device before performing any operation on them.
 - When handling electrical equipment, make sure your hands are dry.
 - Keep all electrical circuit contact points enclosed.
 - If the board becomes too hot try to cool it with a external usb fan.

#### Don’ts:

 - Do not handle the board when it is powered ON.
 - Never touch electrical equipment when any part of your body is wet (that includes fair amounts of perspiration).
 - Do not touch any sort of metal to the development board.

### GPIO

#### Dos:

 - Find out whether the board runs on 3.3v or 5v logic.
 - Always connect the LED (or sensors) using appropriate resistors.
 - To use 5V peripherals with 3.3V we require a logic level converter.

#### Don’ts:

 - Never connect anything greater that 5v to a 3.3v pin.
 - Avoid making connections when the board is running.
 - Do not plug anything with a high (or negative) voltage.
 - Do not connect a motor directly, use a transistor to drive it.

## Using Interfaces (UART,I2C,SPI)

### UART

 - Connect Rx pin of device1 to Tx pin of device2 ,similarly Tx pin of device1 to Rx pin of device2.
 - If the device1 works on 5v and device2 works at 3.3v then use the level shifting (voltage divider) mechanism.
 - Generally UART is used to communicate with board through USB to TTL connection. USB to TTL connection does not require a protection circuit.
 - Whereas Sensor interfacing using UART might require a protection circuit.

### I2C

 - While using I2c interfaces with sensors SDA and SDL lines must be protected.
 - Protection of these lines is done by using pullup registers on both lines.
 - If you use the inbuilt pullup registers in the board you wont need an external circuit.
 - If you are using bread-board to connect your sensor, use the pull-up resistor.
 - Generally , 2.2kohm <= 4K ohm resistors are used.

### SPI

 - Generally ,Spi in development boards is in Push-pull mode.
 - Push-pull mode does not require any protection circuit.
 - On Spi interface if you are using more than one slaves it is possible that the device2 can "hear" and "respond"
 to the master's communication with device1- which is an disturbance.
 - To overcome this problem , we use a protection circuit with pullup resistors on each the Slave Select line(CS).
 - Resistors value can be between 1kOhm ~10kOhm . Generally 4.7kOhm resistor is used.
 
