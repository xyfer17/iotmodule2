# Industrial IoT Protocols
This are some protocols used in IIot.

1. [**4-20mA**](#4-20ma)
2. [**Modbus**](#modbus)
3. [**OPCUA**](#opcua)

## [4-20mA](https://www.predig.com/sites/default/files/documents/whitepapers/4-20_mA_Current_Loop_Fundamentals.pdf)
The 4-20 mA current loop is the prevailing process control signal in many industries. It is an ideal method of transferring process information because current does not change as it travels from transmitter to receiver.
<div align="center">![current](/assignments/summary/assets/currentloop.png)</div>

#### Pros & Cons of 4-20 mA Loops
|Pros|Cons|
|------|-----|
|The 4-20 mA current loop is the dominant standard in many industries.|Current loops can only transmit one particular process signal.|
|It is the simplest option to connect and configure.|Multiple loops must be created in situations where there are numerous process variables that require transmission.|
|It uses less wiring and connections than other signals, greatly reducing initial setup costs.|These isolation requirements become exponentially more complicated as the number of loops increases.|

## [Modbus](https://en.wikipedia.org/wiki/Modbus)
**Modbus** is a communication protocol for use with programmable logic controllers (PLC). It is typically used to transmit signals from instrumentation and control devices back to a main controller, or data gathering system.

#### [How does Modbus Protocols Works?](https://realpars.com/modbus-protocol/)
* The device requesting information is called “master” and “slaves” are the devices supplying information.
* In a standard Modbus network, there is one master and up to 247 slaves, each with a unique slave address from 1 to 247.
* Communication between a master and a slave occurs in a frame that indicates a function code.
The function code identifies the action to perform, such as read a discrete input; read a first-in, first-out queue; or perform a diagnostic function.
* The slave then responds, based on the function code received.

<div align="center">![Modbus](/assignments/summary/assets/modbus.png)</div>


The protocol is commonly used in IoT as a local interface to manage devices.
Modbus protocol can be used over 2 interfaces:

1. RS485 - called as Modbus RTU
2. Ethernet - called as Modbus TCP/IP

#### What is RS-485?
* RS485 is a serial (like UART) transmission standard, you can put several RS485 devices on the same bus.
* RS485 is not directly compatible: you must use the correct type of interface, or the signals won't go through. Mainly done through an easy to use an RS485 to USB.

## OPCUA
OPC Unified Architecture(OPC UA) is a more secure, open, reliable mechanism for transferring information between Servers and Clients. It provides more open transports, better security and a more complete information model than the original OPC DA, “OPC Classic.”

<div align="center">![Modbus](/assignments/summary/assets/opcua.jpg)</div>


#### Why use OPCUA
* It  s a machine to machine communication protocol for Industrial Automation developed by the OPC Foundation.
* The UA APIs are available in several programming languages including C, C++, Java, Python, JavaScript and .NET. It is also platform-independent.
* Comprehensive information modeling for defining complex information.


# Cloud Protocols


### 1. MQTT - Message Queuing Telemetry Transport

 - A simple messaging protocol for constrained devices with low bandwidth.
 - A lightweight publish-and-subscribe system to publish and receive messages as a client.
 - It is the standard messaging and data exchange protocol for IoT.

Communication between several devices can be established simultaneously.
#### Overview

![MQTT Overview](/assignments/summary/assets/mqtt_overview.png)

 - `MQTT Client` as a publisher sends a message to the `MQTT Broker`.
 - The broker distributes the message accordingly to all other MQTT clients subscribed to the topic on which the publisher publishes the message.
 - `Topics` are a way to register interest for incoming messages or to specify where to publish the message. Represented by strings, separated by forward slash. Each slash indicates a topic level.

### 2. HTTP - Hyper Text Transfer Protocol


 - It is a request-response protocol.
 - The client sends an HTTP request.
 - The server returns an HTTP response.

#### The Request

![HTTP](/assignments/summary/assets/http.png)

It has 3 parts:
 - Request line
 - HTTP headers
 - Message body                      

````
GET / HTTP/1.1
Host: xkcd.com
Accept: text/html
User-Agent: Mozilla/5.0 (Macintosh)  
````
There are different types of methods:

**GET** : Retrieve the resource from the server (e.g. when visiting a page)

**POST** : Create a resource on the server (e.g. when submitting a form)

**PUT/PATCH** : Update the resource on the server (used by APIs)

**DELETE** : Delete the resource from the server (used by APIs)

#### The Response

It comprises of 3 parts:
 - Status line
 - HTTP header
 - Message body

```
HTTP/1.1 200 OK
Date: Sat, 02 Apr 2011 21:05:05 GMT
Server: lighttpd/1.4.19
Content-Type: text/html
<html>
    <!-- ... HTML for the xkcd comic -->
</html>
```

The server sends different status codes depending on the situation,Few are listed below.

![Status Codes](/assignments/summary/assets/http_status.jpg)
